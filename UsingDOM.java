// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.sf.saxon.s9api.DOMDestination;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.FeatureNotRecognizedException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.cores9api.ComparisonCancelledException;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelinedComparatorS9;

/**
 * A class that illustrates how to use DOM models in comparison provided by the com.deltaml.cores9api package.  
 */
public class UsingDOM {
  
  /**
   * The document builder factory to use.
   */
  static DocumentBuilderFactory DBF= DocumentBuilderFactory.newInstance();

  /**
   * The document builder used to create new documents and load existing documents.
   */
  private final DocumentBuilder docBuilder;
  
  /**
   * Create the Using DOM sample comparison object.
   * @throws ParserConfigurationException thrown when there is a problem with the parser's configuration.
   */
  public UsingDOM() throws ParserConfigurationException {
    docBuilder= DBF.newDocumentBuilder();
  }
  
  /**
   * The DOM document comparison method.
   * @param doc1 The first document to compare.
   * @param systemId1 The first document's system id (i.e. file location).
   * @param doc2 The second document to compare.
   * @param systemId2 The second document's system id (i.e. file location).
   * @return The document that contains the result of the comparison.
   * @throws SaxonHEConfigurationException thrown when the use of a SaxonHE processor needs to be configured.
   * @throws ParserInstantiationException thrown when there is a problem instantiating or configuring a SAX parser.
   * @throws ComparatorInstantiationException thrown when there is a problem instantiating or configuring a DeltaXML Comparator.
   * @throws IllegalArgumentException thrown when an illegal argument is detected.
   * @throws FeatureNotRecognizedException thrown when a comparison feature is not recognised. 
   * @throws SaxonApiException thrown thrown when there is a problem detected by Saxon's API
   * @throws FilterProcessingException thrown when there is a problem running a pipeline filter.
   * @throws ComparisonException thrown when there is a problem performing the comparison.
   * @throws LicenseException thrown when there is a problem with DeltaXML license.
   */
  public Document compare(Document doc1, String systemId1, Document doc2, String systemId2) throws
      ParserInstantiationException, ComparatorInstantiationException, IllegalArgumentException, FeatureNotRecognizedException,
      SaxonApiException, FilterProcessingException, ComparisonException, LicenseException, ComparisonCancelledException {
    
    // Construct and configure a Saxon processor and DeltaXML (s9api) pipelined comparator.
    // engine due to licensing conditions.
    Processor proc= new Processor(false);
    PipelinedComparatorS9 pc= new PipelinedComparatorS9(proc);
    pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
    pc.setComparatorFeature("http://deltaxml.com/api/feature/deltaV2", true);

    // Appropriately prepare the inputs for comparison using Saxon XdmNodes 
    DOMSource source1= new DOMSource(doc1, systemId1);
    DOMSource source2= new DOMSource(doc2, systemId2);
    XdmNode xdmNode1= proc.newDocumentBuilder().build(source1);
    XdmNode xdmNode2= proc.newDocumentBuilder().build(source2);

    // Perform the comparison and output the result to the destination   
    XdmNode result= pc.compare(xdmNode1, xdmNode2);
    
    // Convert the result to a DOM document and return it
    Document doc= docBuilder.newDocument();
    DOMDestination dest= new DOMDestination(doc);
    proc.writeXdmValue(result, dest);    
    return doc;
  }
  
  /**
   * Loading an existing document from a character stream.
   * @param reader The reader containing the source XML character stream.
   * @return A DOM Document.
   * @throws SAXException thrown when a SAX error is detected.
   * @throws IOException thrown when there is a problem with the IO handling.
   */
  public Document loadDocument(Reader reader) throws SAXException, IOException {
    if( !(reader instanceof BufferedReader) ) {
      reader= new BufferedReader(reader);
    }
    return docBuilder.parse(new InputSource(new BufferedReader(reader)));
  }
  
  /**
   * Save the result of the comparison.
   * @param doc The document to save.
   * @param writer The character stream to write the document to.
   * @throws TransformerException thrown if there is a problem transforming the document into an XML character stream.
   */
  public void saveDocument(Document doc, Writer writer) throws TransformerException {
    TransformerFactory tf= TransformerFactory.newInstance();
    Transformer t= tf.newTransformer();
    t.transform(new DOMSource(doc), new StreamResult(writer));
  }
  
  /**
   * The main program.
   * @param args Two input files and an output file.
   * @throws ParserConfigurationException thrown when there is a problem with the parser's configuration.
   * @throws SAXException thrown when a SAX error is detected.
   * @throws IOException thrown when there is a problem with the IO handling.
   * @throws SaxonHEConfigurationException thrown when the use of a SaxonHE processor needs to be configured.
   * @throws FilterProcessingException thrown when there is a problem running a pipeline filter.
   * @throws ComparisonException thrown when there is a problem performing the comparison.
   * @throws LicenseException thrown when there is a problem with DeltaXML license.
   * @throws ParserInstantiationException thrown when there is a problem instantiating or configuring a SAX parser.
   * @throws ComparatorInstantiationException thrown when there is a problem instantiating or configuring a DeltaXML Comparator.
   * @throws FeatureNotRecognizedException thrown when a comparison feature is not recognised. 
   * @throws IllegalArgumentException thrown when an illegal argument is detected.
   * @throws SaxonApiException thrown thrown when there is a problem detected by Saxon's API
   * @throws TransformerException thrown if there is a problem transforming the document into an XML character stream.
   */
  public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException,
      FilterProcessingException, ComparisonException, LicenseException,
      ParserInstantiationException, ComparatorInstantiationException, FeatureNotRecognizedException, IllegalArgumentException,
      SaxonApiException, TransformerException, ComparisonCancelledException {
    if (args.length != 3) {
      System.out.println();
      System.out.println("Usage:");
      System.out.println("  java -cp <CP> UsingDOM <input-file-1> <input-file-2> <output-file>");
      System.out.println("where <CP> is");
      System.out.println("  ../../deltaxml.jar:../../saxon9pe.jar");
      System.out.println();
      System.exit(1);
    }
    UsingDOM usingDOM= new UsingDOM();
    
    Document doc1= usingDOM.loadDocument(new FileReader(args[0]));
    Document doc2= usingDOM.loadDocument(new FileReader(args[1]));
    
    Document doc3= usingDOM.compare(doc1, args[0], doc2, args[1]);
    
    usingDOM.saveDocument(doc3, new FileWriter(args[2]));
  }
}
