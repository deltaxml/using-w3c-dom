# Using W3C DOM

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

*A simple code sample showing how to use in memory DOM structures as inputs/outputs.*

This document describes how to run the sample. For concept details see: [Using W3C DOM Documents](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-w3c-dom-documents)

The sample code essentially loads two input files (`input1.xml` and `input2.xml`) into DOM documents, compares them, produces an output DOM documents, which is then saved to a file (`output.xml`). 

The sample is designed to be built and run via the Ant build technology. The provided build.xml script has two main targets:

*  run - the default target which compiles and runs the sample code.
*  clean - returns the sample to its original state.  


It can be run by issuing either the `ant` or the `ant run` commands.

Alternatively, the sample can be manually compiled and run using the following Java commands, assuming that both the Java compiler and runtime platforms are available on the command-line, replacing x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`:  


```
javac -cp ../../deltaxml-x.y.z.jar:../../saxon9pe.jar UsingDOM.java
java -cp .:../../deltaxml-x.y.z.jar:../../saxon9pe.jar UsingDOM input1.xml input2.xml output.xml
```

Note that you need to ensure that you use the correct directory and class path separators for your operating system.

See the commentary in the `UsingDOM.java` source file.
